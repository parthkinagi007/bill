function popupActive() {
    $('.joindUs,.jsSignUp').click(function() {
        $('.joinUsPopup').toggleClass('active');
        $('body').toggleClass('mainBlur');
    });

    $('.joinUsPopup .head .close-btn').click(function() {
        $('.joinUsPopup').removeClass('active');
        $('body').removeClass('mainBlur');
    });

    // --------------------------------------------

    $('.js-policy').click(function() {
        $('.jsPrivacyPolicy').toggleClass('active');
        $('body').toggleClass('mainBlur');
    });

    $('.jsPrivacyPolicy .head .close-btn').click(function() {
        $('.jsPrivacyPolicy').removeClass('active');
        $('body').removeClass('mainBlur');
    });

    // --------------------------------------------

    $('.js-disclaimer').click(function() {
        $('.jsDisclaimer').toggleClass('active');
        $('body').toggleClass('mainBlur');
    });

    $('.jsDisclaimer .head .close-btn').click(function() {
        $('.jsDisclaimer').removeClass('active');
        $('body').removeClass('mainBlur');
    });
};

function mobileMenu() {
    $('.nav .menu-btn').click(function() {
        $('.nav').toggleClass('active');
    });
    if ($('.mobile').length != 0) {

        $('#menu .item').on('click', function() {
            $(this).parents('.nav').removeClass('active');
        });
    }
};

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


function joinUs() {
    var frm = $("#joinUsForm");
    $(".msgShowing .waiting").hide()
    $(".msgShowing .err").html("").hide();
    $(".msgShowing .success").html("").hide();

    frm.on('submit', function(e) {
        e.preventDefault();
        // alert('submitted');
        console.log(e);
        var username = frm.find("input[name='jname']").val();
        var email = frm.find("input[name='jemail']").val();
        var phone = frm.find("input[name='jphone']").val();
        if (username != '' && username != undefined && username != null && email != '' && email != undefined && email != null && phone != '' && phone != undefined && phone != null) {
            var emailBool = validateEmail(email);
            // console.log(emailBool);
            if (emailBool) {
                $.ajax({
                    type: 'POST',
                    data: {
                        username: username,
                        email: email,
                        phone: phone
                    },
                    url: 'join-us.php',
                    success: function(result) {
                        console.log(result);
                        if (result) {
                            $(".msgShowing .waiting").hide()
                            $(".msgShowing .err").html("").hide();
                            $(".msgShowing .success").html("Mail Sent Successfully !!! Our team will get back to you").show();
                            setTimeout(function() {
                                $(".joinUsPopup").find("button[type='submit']").removeAttr("disabled");
                                $(".msgShowing .waiting").hide();
                                $(".msgShowing .success").hide();
                                $(".msgShowing .err").hide();
                                $('.joinUsPopup').removeClass('active');
                                $('body').removeClass('mainBlur');
                            }, 2000)
                            $('.bs-form input,.bs-form textarea').val('');
                            // $("#reachUsModal").removeClass('in');
                        } else {
                            $(".msgShowing .waiting").hide()
                            $(".msgShowing .err").html("Opps! Try again").show();
                            $(".msgShowing .success").html("").hide();
                            setTimeout(function() {
                                $(".joinUsPopup").find("button[type='submit']").removeAttr("disabled");
                                $(".msgShowing .waiting").hide();
                                $(".msgShowing .success").hide();
                                $(".msgShowing .err").hide();
                                $('.joinUsPopup').removeClass('active');
                                $('body').removeClass('mainBlur');
                            }, 2000)
                        }
                    },
                    error: function(response) {
                        console.log(response);
                    }
                });
            } else {
                $(".msgShowing .waiting").hide()
                $(".msgShowing .err").html("").show();
                $("#reachUsForm .error-cont").text("Please enter the correct email address.");
            }
        } else {
            $(".msgShowing .waiting").hide()
            $(".msgShowing .err").html("").show();
            $("#reachUsForm .error-cont").text("All fields must be completed before you submit the form.");
        }
    });
}

function stayInTouch() {
    var frm = $("#intrestedInquiryForm");
    $(".msgShowing .waiting").hide()
    $(".msgShowing .err").html("").hide();
    $(".msgShowing .success").html("").hide();

    frm.on('submit', function(e) {
        e.preventDefault();
        // alert('submitted');
        console.log(e);
        var username = frm.find("input[name='name']").val();
        var email = frm.find("input[name='email']").val();
        var phone = frm.find("input[name='phone']").val();
        var msg = frm.find("textarea").val();
        console.log(username);
        if (username != '' && username != undefined && username != null && email != '' && email != undefined && email != null && phone != '' && phone != undefined && phone != null && msg != '' && msg != undefined && msg != null) {
            var emailBool = validateEmail(email);
            // console.log(emailBool);
            if (emailBool) {
                $.ajax({
                    type: 'POST',
                    data: {
                        username: username,
                        email: email,
                        phone: phone,
                        msg: msg
                    },
                    url: 'intrested-inquiry.php',
                    success: function(result) {
                        console.log(result);
                        if (result) {
                            $(".msgShowing .waiting").hide()
                            $(".msgShowing .err").html("").hide();
                            $(".msgShowing .success").html("Mail Sent Successfully !!! Our team will get back to you").show();
                            setTimeout(function() {
                                $("button[type='submit']").removeAttr("disabled");
                                $(".msgShowing .waiting").hide();
                                $(".msgShowing .success").hide();
                                $(".msgShowing .err").hide();
                            }, 2000)
                            $('.bs-form input,.bs-form textarea').val('');
                        } else {
                            $(".msgShowing .waiting").hide()
                            $(".msgShowing .err").html("Opps! Try again").show();
                            $(".msgShowing .success").html("").hide();
                            setTimeout(function() {
                                $("button[type='submit']").removeAttr("disabled");
                                $(".msgShowing .waiting").hide();
                                $(".msgShowing .success").hide();
                                $(".msgShowing .err").hide();
                            }, 2000)
                        }
                    },
                    error: function(response) {
                        console.log(response);
                    }
                });
            } else {
                $(".msgShowing .waiting").hide()
                $(".msgShowing .err").html("").show();
                $("#reachUsForm .error-cont").text("Please enter the correct email address.");
            }
        } else {
            $(".msgShowing .waiting").hide()
            $(".msgShowing .err").html("").show();
            $("#reachUsForm .error-cont").text("All fields must be completed before you submit the form.");
        }
    });
}


function fireClick() {
    setTimeout(function() {
        $(".fp-viewing-vendor-0 .bs-sec4 .fp-next").trigger('click');
    }, 10000);
}





$(document).on('ready', function() {
    // if ($('html').hasClass('desktop') == true) {
    // $("#fullpage").remove();
    var myFullpage = new fullpage('#fullpage', {
        sectionsColor: ['#1bbc9b', '#ffffff', '#ffffff', 'whitesmoke', '#ccddff'],
        anchors: ['home', 'billme', 'team', 'vendor', 'contact'],
        menu: '#menu',
        lazyLoad: true,
        scrollingSpeed: 1500,
        slidesNavigation: true
    });
    // }
    popupActive();
    mobileMenu();
    fireClick();

    joinUs();
    stayInTouch();
});

$(window).load(function() {
    $(".loader").addClass("hide");
});